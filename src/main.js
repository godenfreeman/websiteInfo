import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import store from './store/index'
import './plugins/element.js'

import './assets/css/minireset.css'
import './assets/css/index.css'


Vue.config.productionTip = false;

Vue.prototype.$http=axios;

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
