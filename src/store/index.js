import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const store = new Vuex.Store({
	state: {
    // current: window.localStorage.getItem('current') ? parseInt(window.localStorage.getItem('current')) : 0,
    count:1
	},
	mutations: {
		update(state, index) {
			state.current = index;
			window.localStorage.setItem('current', index);
    },
    increment(state){
      state.count++
    },
    decrement(state){
      state.count--
    }
  },
  actions:{
    increment({commit}){
      commit('increment')
    },
    decrement({commit}){
      commit('decrement')
    }
  }
})

export default store

