import axios from 'axios';
import store from '../store/index'
import router from '../router/index'
import { Message } from 'iview';
// axios.defaults.baseURL = "aaaaa"
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
let config = {
    baseURL: "aaaaa",
    // baseURL: "bbbbb",
    timeout: 60 * 1000, // 延时
    // withCredentials: true, // 检查跨域访问控制
};

const _axios = axios.create(config);

//http request 拦截器
_axios.interceptors.request.use(
    function (config) {
        //在发送请求之前做一些事情
        if (store.state.token) {
            config.headers.Authorization = `${store.state.token}`
        }
        return config;
    },
    function (error) {
        //执行请求错误的操作
        return Promise.reject(error);
    }
);

//响应拦截器即异常处理
_axios.interceptors.response.use(
    function (response) {
        // 响应数据
        return response;
    },
    function (err) {
        switch (err.response.status) {
            case 401:
                Message.warning('未授权，请重新登录');
                store.dispatch("loginOutFun");
                // 只有在当前路由不是登录页面才跳转
                router.currentRoute.path !== 'login' &&
                    router.replace({
                        path: 'login',
                        query: { redirect: router.currentRoute.path },
                    })
                break;
            case 500:
                Message.warning("服务器端出错,请刷新页面！");
                break;
            default:
                Message.warning(`连接错误${err.response.status}`);
        }
        // 响应错误
        return Promise.reject(err);
    }
);

/**
 * 封装get方法
 * @param url
 * @param data
 * @returns {Promise}
 */
export function fetch(url, params = {}) {
    return new Promise((resolve, reject) => {
        _axios.get(url, {
            params: params
        })
            .then(response => {
                resolve(response);
            })
            .catch(err => {
                reject(err)
            })
    })
}

/**
 * 封装post请求
 * @param url
 * @param data
 * @returns {Promise}
 */
export function post(url, data = {}) {
    return new Promise((resolve, reject) => {
        _axios.post(url, data)
            .then(response => {
                resolve(response.data);
            }, err => {
                reject(err)
            })
    })
}

/**
 * 封装file请求
 * @param url
 * @param data
 * @returns {Promise}
 */
export function fileUpload(url, fileData) {
    return new Promise((resolve, reject) => {
        axios({
            url,
            method: "post",
            data: fileData
        }).then(response => {
            resolve(response.data);
        }, err => {
            reject(err)
        })

    })
}

const header = "";
export const server = {
    Table: function () {
        return fetch(header + '/Table');
    },
    getRSAParams: function () {
        return fetch('/trace/login/getRSAParams');
    },
    checkTerminalLogin: function (data) {//登录
        return post('/trace/login/checkTerminalLogin',data);
    },
    saveNewPwd(data) {//修改密码
        return post('/trace/login/saveNewPwd', data);
    },
    userInfoByToken(data) {//通过token换用户信息
        return post('/trace/api/common/getUserIdFromToken', data);
    },
    getTemplant(data) {//请求模板结构
        return post('/trace/api/terminal/templant/getTemplant',data);
    },
    submitTemplant(data) {//提交模板
        return post('/trace/api/terminal/templant/submitTemplant',data);
    },
    queryProductInfoTerminal (data) {//查询产品列表NEW
        return post('/trace/api/terminal/product/queryProductInfoTerminal',data);
    },
    getproduceData(data) {//查询产品列表Old
        return post('/trace/api/terminal/produce/getproduceData',data);
    },
    saveProduceInfo(data) {//新建和编辑产品
        return post('/trace/api/terminal/product/saveProductInfo',data);
    },
    deleteProduceInfo(data) {//删除产品
        return post('/trace/api/terminal/product/deleteProductInfo',data);
    },
    queryPolicyInsts(data) {//查询产品批次
        return post('/trace/api/terminal/templant/queryPolicyInsts',data);
    },
    queryPolicyInstsPrint(data) {//查询产品批次二维码
        return post('/trace/api/terminal/templant/queryPolicyInstsPrint',data);
    },
    deleteTraceNbr(data) {//删除产品批次
        return post('/trace/api/terminal/templant/deleteTraceNbr',data);
    },
    getTemplantByTraceNbr(data) {//产品批次编辑
        return post('/trace/api/terminal/templant/getTemplantByTraceNbr',data);
    },
    queryTraceInfoByTraceNbr(data) {//溯源码查询档案详情
        return post('/trace-service/api/templant/queryTraceInfoByTraceNbr',data);
    },
    getTerminalFlowInfo(data){//多级table
        return post('/trace/api/terminal/checkFlow/getTerminalFlowInfo',data);
    },
    checkFlowGetPolicyInfo(data){//校验追溯码
        return post('/trace/api/terminal/checkFlow/getPolicyInfo',data);
    },
    findPageByParam(data) {//"三品一标"认证产品信息
        return post('/trace-service/api/certify/findPageByParam',data);
    },
    getInputs(data) {//投入品列表
        return post('/trace/api/terminal/inputs/getInputs',data);
    },
    getPolicyInstInfo(data) {//投入品参数
        return post('/trace/api/terminal/inputs/getPolicyInstInfo',data);
    },
    getInputsTemplantById(data) {//投入品编辑
        return post('/trace/api/terminal/inputs/getInputsTemplantById',data);
    },
    delInputsTemplantById(data) {//投入品删除
        return post('/trace/api/terminal/inputs/delInputsTemplantById',data);
    },
    submitInputsTemplant(data) {//投入品提交
        return post('/trace/api/terminal/inputs/submitInputsTemplant',data);
    },
    queryRichPage(data) {//公告列表
        return post('/trace/api/contentInfo/queryRichPage',data);
    },
    queryRichById(data) {//公告列表
        return post('/trace/api/contentInfo/queryRichById',data);
    },
    queryBaseInfoTerminal(data) {//企业详情
        return post('/trace/api/terminal/baseInfo/queryBaseInfoTerminal',data);
    },
}