import axios from 'axios'
import {baseConfig} from '../assets/js/config.js';



const apiData = {
    policyList(method, url, obj){
        return new Promise((resolve, reject)=>{
                axios[method](`${baseConfig.dev}${url}`,obj)
                    .then(function (res) {
                        resolve(res)
                    })
                    .catch(err => {
                        reject(err)
                    })
            }
        )
    },
    fetchList(method, url, obj){
      return new Promise((resolve, reject)=>{
          axios[method](`${baseConfig.test}${url}`,obj)
            .then(function (res) {
              resolve(res)
            })
            .catch(err => {
              reject(err)
            })
        }
      )
    }
}





export {
    apiData
}
