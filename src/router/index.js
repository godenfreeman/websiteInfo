import Vue from 'vue'
import Router from 'vue-router'

const Index = r => require.ensure([], () => r(require('@/views/index')), 'Index');
const List = r => require.ensure([], () => r(require('@/views/list')), 'List');
const Detail = r => require.ensure([], () => r(require('@/views/detail')), 'detail');
const Test = r => require.ensure([], () => r(require('@/views/testIndex')), 'test');

Vue.use(Router)

export default new Router({
    // mode:"history",
    routes: [
        {
            path: '/',
            redirect: '/index'
        },
        {
            path: '/index',
            name: 'Index',
            component: Index
        },
        {
            path: '/list',
            name: 'List',
            component: List
        },
        {
            path: '/detail',
            name: 'Detail',
            component: Detail
        },
        {
            path: '/test',
            name: 'Test',
            component: Test
        }
    ]
})
