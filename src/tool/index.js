const tool = {
    padDate(va) {
        va = va < 10 ? '0' + va : va;
        return va
    },
    getItem(key) {
        if (window.localStorage) {
            return window.localStorage.getItem(key);
        } else {
            return Cookie.read(key);
        }
    },
    setItem(key,val){
        if (window.localStorage) {
            localStorage.setItem(key,val);
        } else {
            Cookie.write(key,val);
        }
    },
    findIndex(target, data, param){
        let index = -1;
        data.map((item, key) => {
            if(item[param] === target){
                index = key;
                return
            }
        });
        return index
    }
}

export {
    tool
}
