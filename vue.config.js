module.exports = {
    // 部署应用时的基本 URL
    //publicPath: process.env.NODE_ENV === 'production' ? '192.168.60.110:8080' : '192.168.60.110:8080',
    publicPath: './',
    devServer:{
        port:9081, //端口号
        proxy: {
            '/api':{
                target:'http://27.150.169.28:7020', //目标API地址
                ws:false,
                changeOrigin:true,
                pathRewrite:{
                    "^/api":''
                }
            },
            '/test':{
              target:'http://localhost:7001', //目标API地址
              ws:false,
              changeOrigin:true,
              pathRewrite:{
                "^/test":''
              }
            }
        }
    },
    assetsDir: 'assets',
    lintOnSave: false, //取消eslint验证
};
